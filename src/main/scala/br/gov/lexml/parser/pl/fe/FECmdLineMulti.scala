package br.gov.lexml.parser.pl.fe

import java.io.File
import java.util.Date
import java.io.FileWriter
import java.io.BufferedWriter
import br.gov.lexml.parser.pl.linker.Linker
import java.text.SimpleDateFormat
import br.gov.lexml.parser.pl.profile.DocumentProfileRegister.TipoNorma
import scala.collection.mutable.ListBuffer
import javax.swing.text.rtf.RTFReader
import javax.swing.text.rtf.RTFReader
import javax.swing.text.rtf.RTFReader
import javax.swing.text.rtf.RTFReader
import javax.swing.text.rtf.RTFReader
import javax.swing.text.rtf.RTFReader
import scala.io.Source
import java.io.FileOutputStream
import java.nio.charset.StandardCharsets
import java.io.OutputStreamWriter

object FECmdLineMulti {

  def main(args: Array[String]) = {
    args.foreach(println)
    val listFiles = getFileTree(new File("rtf")).filter(_.getName.endsWith(".rtf")).filter(x => (x.getName.startsWith("MPV")))// || x.getName.startsWith("LEI")))
    val logGeral = new File("log")
    if (!logGeral.exists()) logGeral.mkdirs();
    val logFile = "log%slog-%s.txt".format(File.separator, new SimpleDateFormat("dd-MM-YYY-HHmmss").format(new Date()))
    var fileNumber = 1
    log("%d arquivos encontrados".format(listFiles.length))

    listFiles.foreach(parse)
    Linker.system.shutdown()
    log("Fim do processamento.")

    def parse(file: File) = {

      val split = file.getName.replace(".rtf", "").split("[-]")
      val tipo = split(0)
      val ano = split(1)
      val numero = split(2)
      var complemento = ""
      if (split.length > 3) complemento = split(3)

      println(complemento)

      log("Iniciando parsing do arquivo  %d de %d - %s".format(fileNumber, listFiles.length, file.getName))
      val logDir: File = new File(getDir("log", ano))
      if (!logDir.exists()) logDir.mkdirs()
      val txtDir = new File(getDir("txt", ano))
      if (!txtDir.exists()) txtDir.mkdirs()
      val lexmlDir = new File(getDir("lexml", ano))
      if (!lexmlDir.exists()) lexmlDir.mkdirs()

      val argsFE: ListBuffer[String] =
        ListBuffer("parse",
          "-m",
          "text/rtf",
          "-i",
          file.getAbsolutePath,
          "-o",
          "%s%s".format(getDir("lexml", ano), file.getName.replace(".rtf", ".xml")),
          "--write-errors-to-file",
          "%s%s".format(getDir("log", ano), file.getName.replace(".rtf", ".log")),

          /* "--linker",
          new File(s"linker-dist${File.separator}build${File.separator}simplelinker${File.separator}simplelinker").getAbsolutePath,
          */
          "-l",
          "br",
          "-t",
          "lei",
          "-a",
          "federal",
          "-n",
          numero,
          //"--complemento",
          //complemento,
          "--ano",
          ano,
          "--prof-epigrafe-opcional",
          "--prof-pre-epigrafe-permitida",
          
          //"--prof-regex-epigrafe",
          
          //"^\\s*(red\\d+;+)?(projeto( de)? (lei|decreto legislativo)|(proposta|projeto) de emenda|pec|projeto de resolu)%^(n[oº°˚]|complementar)",
    
         // "--prof-regex-preambulo",
          //"a p r e s i d e n t a d a r e p u b l i c a",
          
          "-v")
      if (args.length > 0) {
        argsFE += "--linker"
        argsFE += args(0)
      }
      println(argsFE.toString())
      try {
       // for (line <- Source.fromFile(file).getLines()) {
          //if (line.contains("##")) println(line) else writeToFile(file.getName,line)
        //}
        FECmdLine.main(argsFE.toArray);
        log("Fim do parsing do arquivo %s ".format(file.getName))
      } catch {
        case e: Exception =>
          log("Erro no parsing do arquivo %s".format(file.getName))
          log(e.getMessage)
          e.printStackTrace()
      } finally {
        fileNumber = fileNumber + 1
      }
    }

    def log(message: String) = {
     println(message)
      val w = new BufferedWriter(new FileWriter(logFile, true))
      try {

        w.write(("%s: %s".format(new SimpleDateFormat("dd-MM-YY-HH:mm:ss").format(new Date()), message)))
        w.newLine()
      } finally {
        w.close()
      }
    }

  }
  def writeToFile(txtFile: String, message: String) = {
    println(message)
    val w = new OutputStreamWriter(new FileOutputStream(new File(txtFile),true), StandardCharsets.UTF_8)
    
    //val w = new BufferedWriter(new FileWriter(txtFile, true))
    
    try {
      w.append(message+"\n")
     
    } finally {
      w.close()
    }
  }
  def getDir(pasta: String, ano: String): String = {
    "%s%s%s%s".format(pasta, File.separator, ano, File.separator)
  }
  def getFileTree(f: File): Stream[File] =
    f #:: (if (f.isDirectory) f.listFiles().toStream.flatMap(getFileTree)
    else Stream.empty)

}
